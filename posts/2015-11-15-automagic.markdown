---
title: Automagic
author: Zach Oglesby
date: 2015-11-15
---

Using a static blogging engine like Hakyll is great, but it also means that I can't post blog entires from a we browser, my phone, or tablet. That issue has now been fixed, thanks to the magic of git hooks, I am now able to upload the source of this site to my webserver and have it build and publish new content. It depends on git, but there are good solutions for both iOS and Android for that. Then all you need is a good text editor and volla, static blogging on a mobile device. Take that WordPress!

Git post-recieve code

	#!/bin/bash                                                                                                                                                               
	echo 'Doing the build'
	git --work-tree=/path/to/build --git-dir=/path/to/blog checkout -f
	ghc --make /path/to/build/site.hs
	cd /path/to/build/ && ./site build
	rsync -avcz /path/to/build/_site/ /var/www/site/public_html/
