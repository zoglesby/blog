---
title: Goose Project
date: September  1, 2012
author: Zach Oglesby
---

<p>Earlier this year I was at <a href="http://www.southeastlinuxfest.org/">South East Linux Fest</a> in Charlotte NC, and listened to a talk by Clint Savage (<a href="http://herlo.org">Herlo</a>) about a new Enterprise Linux rebuild project that he was working on. The goal of the project was to make a community based enterprise rebuild that operates very similar to <a href="http://fedoraproject.org">Fedora</a>. I was intrigued, like most people the giant gap between RHEL 6 and CentOS 6 was frustrating to me; the idea of a community run enterprise distro sounded great.</p>
<p>The question was how can I help? The answer was &quot;What are you good at?&quot; Since <a href="http://gooseproject.org">GoOSe</a> is just getting off the ground they need help all over. Naturally I started working on documentation stuff, but because its a small community the team needed someone with PostgreSQL experience so I started helping with that as well.</p>
<p>What I am doing is not the point of this post. The point I am trying to make is that in a large community like Fedora, it can be hard to find a task that interests you, someone is already packaging your favorite stuff, or people are working on infrastructure you know and it can be intimidating to try and jump in.</p>
<p>GoOSe is a new project and there is plenty of stuff for people to work on. It is also a small group of people working on it at them moment which makes it that much easier to get acquainted and feel comfortable working with the group.</p>
<p>If this sounds interesting to you, or you want to hear more jump in #gooseproject on freenode, or visit the <a href="http://gooseproject.org">website</a>. You can also see source code on <a href="http://github.com/gooseproject">github</a></p>
