---
title: Gmail+
date: January 13, 2014
author: Zach Oglesby
---
    
<p><a href="http://www.theverge.com/">The Verge</a> posted an <a href="http://www.theverge.com/2014/1/13/5305034/high-profile-google-users-will-get-better-default-email-settings">article</a> today explaining how Google has made the decision to exclude popular Google+ users from their new Gmail/Google+ integration. Most users of Google's mail services are now contactable via Google+ search, meaning that anyone can look a person up via Google+ and send them an email. The only way to stop that from happening is to opt-out of the new feature; something that most people will not do.</p>
<p>The fact that Google is making special allowances for popular users shows that they realize the negative aspects of this new &quot;feature&quot;, but rather than role back the change they are making it opt-in for a smaller subset of users.</p>
<p><a href="http://en.wikipedia.org/wiki/Don't_be_evil">Don't be evil</a> is dead.</p>
