---
title: Pintail Talk
author: Zach Oglesby
date: Tue, 16 Aug 2016 09:28:00 -0400
tags: fedora 
--- 

In case you are not following me on twitter, and I don't know why you would be. [Here is the talk][2] that [Shaun][1] gave at [GUADEC][3].


https://media.ccc.de/v/108-unconference-6

[1]: https://mobile.twitter.com/shaunm
[2]: https://media.ccc.de/v/108-unconference-6
[3]: https://2016.guadec.org