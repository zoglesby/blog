---
title: Need to Know
date: October 12, 2013
author: Zach Oglesby
---


<p>If this is not scary I don't know what is…</p>
<blockquote>
<p>The government’s response (PDF), filed on September 30th, is a heavily redacted opposition arguing that when law enforcement can monitor one person’s information without a warrant, it can monitor everyone’s information, “regardless of the collection’s expanse.” Notably, the government is also arguing that <strong>no one other than the company that provided the information—including the defendant in this case—has the right to challenge this disclosure in court.</strong></p>
</blockquote>
<p><a href="http://arstechnica.com/tech-policy/2013/10/doj-if-we-can-track-one-american-we-can-track-all-americans/" class="uri">http://arstechnica.com/tech-policy/2013/10/doj-if-we-can-track-one-american-we-can-track-all-americans/</a></p>
