---
title: "Re: Automagic"
author: Zach Oglesby
date: 2016-03-02
---

I recently moved my git repos to an instance of [gitlab][gl], at the same time I figured I would try to start using its built-in [CI][ci] tool to build and publish my blog. Now when code is checked into the main repo it is built, tested, and deployed. I don't have to remember to setup a second repo on each server or push to it. Thanks to docker (Yes, I know), I can build it in a sandbox and tear it down when I am down. 

Here is my [.gitlab-ci.yml][yml] file as an example.
        
	image: zoglesby/hakyll-image:v3

        before_script:
          - cabal update
          - cabal install blog.cabal
        
        production:
          type: deploy
          script:
            - blog build
            - blog deploy

That code uses a docker image I have setup with [haskell][hask] and [hakyll][hak], updates cabal and installs my [cabal][cab] package. Then it build the blog and deploys it. The first few times I ran this I did not have hakyll in the prebuilt docker image and it was taking 20 minutes or so to build, now it runs in less than a minute.


[gl]: https://about.gitlab.com
[yml]: http://doc.gitlab.com/ce/ci/yaml/README.html
[cab]: https://www.haskell.org/cabal/
[hask]: https://www.haskell.org
[hak]: https://jaspervdj.be/hakyll/
[ci]: https://about.gitlab.com/gitlab-ci/
