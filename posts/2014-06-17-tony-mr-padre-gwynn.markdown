---
title: Tony "Mr. Padre" Gwynn
date: June 17, 2014
author: Zach Oglesby
---

<p>Yesterday Tony Gwynn died, as a child of San Diego and a Padres fan my whole life it was a shock and a blow. I normally don't think about the deaths of famous people but Tony Gwynn was a class act and a great member of the San Diego community.</p>
<iframe width="560" height="315" src="//www.youtube.com/embed/fUlEuv4u65Q" frameborder="0" allowfullscreen>
</iframe>
<iframe width="560" height="315" src="//www.youtube.com/embed/7PXSVbLhyMY" frameborder="0" allowfullscreen>
</iframe>
