---
title: GoOSe, CentOS and Red Hat
date: January 17, 2014
author: Zach Oglesby
---

<p>Open Source projects are hard, until you you reach a critical mass of people working on a project there is on guaranty that it will be successful. I think that is what happend with <a href="http://gooseproject.org">GoOSe Linux</a>. We worked hard to release GoOSe 6.0 and updates, but after that the small community of people all got busy and work basically came to a halt.</p>
<p>The news about <a href="http://lists.centos.org/pipermail/centos-announce/2014-January/020100.html">CentOS joining forces with Red Hat</a> was great for me because of that. In an interview with <a href="https://www.linux.com/news/featured-blogs/200-libby-clark/757524-centos-project-leader-karanbir-singh-opens-up-on-red-hat-deal/">Linux.com</a> Karanbir Singh talked about how they want to open up CentOS development, creating a real Community Enterprise Operating System.</p>
<blockquote>
<p>“We'll be working on infrastructure other projects need to be successful with us,” Singh said. “We won't be delivering the features. We'll make it as trivial as possible to come in and do the build you need, without needing to learn everything about building CentOS images.”</p>
</blockquote>
<p>This was the goal of GoOSe and I am glad to see that CentOS will be able to move towards that goal. I hope that in a few years (hopefully sooner) CentOS will be just as open of a community as Fedora.</p>
