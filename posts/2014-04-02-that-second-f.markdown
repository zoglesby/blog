---
title: That second F
date: April  2, 2014
author: Zach Oglesby
---

<p>I have been using Fedora for a long time, what started out as the simple usage of an operation system turned into a passion for a great community. I have always thought that the 4 Fs of Fedora where a catchy yet honest representation of the project, but recently the 2nd F has had a whole new meaning to me.</p>
<p>In January my wife and I had our third child who was born with several medical problems. I made a passing comment to someone about it in IRC and I have been blown away by the amount of support I have received since that time. Someone once told me that users are great but making users contributors is what strengthens the Fedora Project. If Fedora had all the users in the world, but no new contributors where being added everything would fall apart.</p>
<p>That idea is obvious and true, but now more than ever I see how important the idea of friends are to the community. Over the years I have made many friends in the Fedora community and that has only strengthened my passion for Fedora. When you receive so much support from people who you only talk to online, in a time of need, it is an amazing thing. People I do not normally talk to regularly reached out to me to offer support or sometimes just to talk. I think that the amount of support I have from members of the Fedora community may have been better than what I had from people I deal with in person on a daily basis.</p>
<p>This post is not to talk about my issues, my son is doing better, it is to highlight all of the amazing people we have working on Fedora every day. Fedora is made up of a wonderful community of smart and helpful people. While I think that freedom, features, and being first are all valuable attributes for Fedora to aim for, to me the friendship is the most important.</p>
