---
title: Thank you, Seth
date: Posted on July 17, 2013
author: Zach Oglesby
---

<p>I don't really have the words to say this, so I will just link to Red Hats post. This was sad news indeed.</p>
<blockquote>
<p>Collaboration and community are truly at the heart of everything Red Hat does. Seth Vidal, a longtime member of the Fedora Project and Red Hat’s Fedora team, espoused these values and represented the best of open source. He was a lead developer of the yum project, the software package manager used by Fedora, Red Hat Enterprise Linux, and other RPM-based distributions. He played a significant role in the Fedora infrastructure team, working tirelessly to keep the lights on and leading the efforts to make building and managing third-party package repositories easy for Fedora developers. With permission from Seth’s loved ones, it is with great sadness that we share that Seth died tragically on July 8. The entire Red Hat family extends our sympathies to Seth’s family and friends during this difficult time. In the last 24 hours, Red Hatters from around the world have expressed their condolences and remembered Seth on memo-list, Red Hat’s infamous internal mailing list where Seth himself was a regular and passionate contributor. We’ve seen Seth described as funny, smart, charming, sometimes opinionated, and always a fervent supporter of open source. Seth’s contributions span far and wide in the open source community, and his impact will live on through the millions of people around the world who touch open source each day. Thank you, Seth, for everything you contributed to open source, to Fedora, and to Red Hat. We will miss you, and you will never be forgotten.</p>
</blockquote>
<p><a href="http://www.redhat.com/about/news/archive/2013/7/thank-you-seth-vidal" class="uri">http://www.redhat.com/about/news/archive/2013/7/thank-you-seth-vidal</a></p>
