---
title: Android Security
date: October 14, 2013
author: Zach Oglesby
---

<p>As a reminder, Android is open source but is not a community project; there is a difference. Please try and submit a patch for this and see how far it gets you.</p>
<blockquote>
<p>Some time ago, I was adding secure authentication to my APRSdroid app for Amateur Radio geolocation. While debugging its TLS handshake, I noticed that RC4-MD5 is leading the client's list of supported ciphers and thus wins the negotiation. As the task at hand was about authentication, not about secrecy, I did not care.</p>
</blockquote>
<p><a href="http://op-co.de/blog/posts/android_ssl_downgrade/" class="uri">http://op-co.de/blog/posts/android_ssl_downgrade/</a> via <a href="http://sparkslinux.wordpress.com/2013/10/14/why-android-ssl-was-downgraded-from-aes256-sha-to-rc4-md5-in-late-2010/" class="uri">http://sparkslinux.wordpress.com/2013/10/14/why-android-ssl-was-downgraded-from-aes256-sha-to-rc4-md5-in-late-2010/</a></p>
